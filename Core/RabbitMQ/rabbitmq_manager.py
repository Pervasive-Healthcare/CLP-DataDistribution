import pika
import json

from Core.RabbitMQ.message_sender import init_channel

filename = None 

#######################################################################################################################
#                                           RABBITMQ CALLBACKS
#######################################################################################################################
def get_filename():
    return filename

def set_filename():
    global filename
    filename = None

def find_json_filename(ch, method, properties, body):
    global filename
    filename = json.loads(body)

def init_rabbitmq(rabbitmq_config):
    global channel
    credentials = pika.PlainCredentials(rabbitmq_config.get('user'), rabbitmq_config.get('password'))
    parameters = pika.ConnectionParameters(rabbitmq_config.get('host'),
                                           rabbitmq_config.get('port'),
                                           '/',
                                           heartbeat=0,
                                           credentials=credentials)
    


    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.queue_declare(queue='req_to_repo')
    channel.queue_declare(queue='json_filename')
    channel.basic_consume(queue='json_filename',on_message_callback=find_json_filename,auto_ack=True)

    init_channel(channel)
    try:
        channel.basic_qos(prefetch_count=10)
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    connection.close()