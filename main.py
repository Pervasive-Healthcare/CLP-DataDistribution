import json
import threading

import pika
import yaml
from flask import Flask, request, Response, jsonify
from flask_cors import CORS
from io import BytesIO
import re

from Core.RabbitMQ.message_sender import send_message, init_channel
from Core.RabbitMQ.rabbitmq_manager import init_rabbitmq, get_filename, set_filename
from Utils.enums import Config, DefaultGeneralQuery
from Utils.json_validator import validate_json, JsonSchema
from Utils.model_rpc_client import ModelRpcClient
from Utils.classifier_builder_utils import zip_when_model_found, train_cycle
from Utils.dockerhelper import init_docker_client, run_models

import requests
from zipfile import ZipFile
import os
from datetime import datetime

with open("config.yml", "r") as yml_file:
    config = yaml.safe_load(yml_file)

with open("docker_config.yml",'r') as yml_docker:
    docker_config = yaml.safe_load(yml_docker)
    for i in docker_config['ports'].keys():
        docker_config['ports'].get(i).append(datetime.timestamp(datetime.now()))
    docker_config["running_cont"] = 0

init_docker_client()

rabbitmq_config = config[Config.RABBIT_MQ_CONNECTION.value]
app = Flask(__name__)
CORS(app)

rpc_client = ModelRpcClient() #Oggetto per comunicazione con RepManagement

def flask_thread():
    app.run(host="0.0.0.0", port=5003)  

#######################################################################################################################
#                                               /download_data
#######################################################################################################################
@app.route("/download_data", methods=["POST"])
def trigger_query():
    if request.method == "POST":
        query = request.get_json()
        print(query)
        if not validate_json(query, JsonSchema.QUERY_SCHEMA, "query"):
            return {"download_started": "failed"}

        query["type"] = "query"

        credentials = pika.PlainCredentials(rabbitmq_config.get('user'), rabbitmq_config.get('password'))
        parameters = pika.ConnectionParameters(rabbitmq_config.get('host'),
                                               rabbitmq_config.get('port'),
                                               '/',
                                               heartbeat=1800,
                                               credentials=credentials)
        

        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.basic_publish(exchange='', routing_key='req_to_repo', body=json.dumps(query))
        f = get_filename()
        while (f == None):
            f = get_filename()
        connection.close()

        if request.remote_addr == "127.0.0.1":
            ip = "localhost"
        else:
            r = requests.get(r'http://jsonip.com')
            ip= r.json()['ip']

        download_link = "http://" + ip +":5001/download_json/" + f
        set_filename()
        return {
                "download_started": "successfully",
                "format": "JSON",
                "sensors": query.get("sensors"),
                "labels": query.get("labels"),
                "positions": query.get("positions"),
                "devices": query.get("devices"),
                "users": query.get("users"),
                "download_link": download_link
            }


#######################################################################################################################
#                                               /deployClassifier
#######################################################################################################################
@app.route("/deployClassifier", methods=["POST"])
def deployClassifier():
    if request.method == "POST":
        body = request.get_json()
        print(body)
        if not validate_json(body, JsonSchema.GET_COMPONENT_SCHEMA, "body"):
            return {"deployClassifier": "failed"}

        req = requests.post('http://localhost:5003/get_component/0', json = body)

        if req.headers.get('Content-Type') == 'application/zip':
            info_file = re.findall("filename=(.+)", req.headers['Content-Disposition'])[0]
            list_model = []
            with ZipFile(BytesIO(req.content), 'r') as zipObj:
                listFile = zipObj.namelist()
                with zipObj.open('query.json') as jsonFile:
                    queryDict = json.loads(jsonFile.read().decode())
                # Modello da aggiungere al db
                if queryDict['new_model']:
                    for fileName in zipObj.namelist():
                        if not fileName.endswith('.json'):
                            zipObj.extract(fileName, 'models')
                            list_model.append(fileName)

                    body_dict = dict()
                    body_dict['query_gen'] = queryDict
                    body_dict['model_name'] = list_model
                    body_dict['framework'] = info_file.split(".")[0].split("_",1)[0]
                    body_dict['datatime'] = info_file.split(".")[0].split("_",1)[1]
                    body_dict['type'] = "model_put"
                    res = rpc_client.call(body_dict)
                    res = json.loads(res.decode())
                    if res['results'] == 'error':
                        try:
                            for fileModel in list_model:
                                os.unlink(os.path.abspath(os.path.join('models', fileModel)))
                        except OSError as e:
                            print ("Error: %s - %s." % (e.filename, e.strerror))
                        finally:
                            return {"deployClassifier": "failed"}
                    else:
                        return run_models(docker_config,body_dict['model_name'], body_dict['framework'])
                # Modello nel db        
                else:
                    listFile.remove('query.json')
                    return run_models(docker_config, listFile, queryDict.get('framework'))
        elif req.headers.get('Content-Type') == 'application/json':
            return json.loads(req.content.decode('UTF-8'))
    return {"status":"error"}
        


#######################################################################################################################
#                                               /get_component/{tf_format}
#######################################################################################################################
@app.route("/get_component/<tf_format>", methods=["GET", "POST"])
def get_component(tf_format):
    if request.method == "GET":
        
        if tf_format != '0' and tf_format != '1':
            return {"error" : "invalid tf_format"}

        query = DefaultGeneralQuery.DEFAULT_GENERAL_QUERY.value

        folder_name = "general_model_" + (str)(datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f'))

        data = train_cycle(tf_format, folder_name, "TF_general_model", query)
        if not data:
            return {"error":"no data in db"}
        else:
            zip_name = folder_name + ".zip"
            return Response(data, headers={
                'Content-Type': 'application/zip',
                'Content-Disposition': 'attachment; filename=%s;' % zip_name})
    elif request.method == "POST":

        if tf_format != '0' and tf_format != '1':
            return {"error" : "invalid tf_format"}

        body = request.get_json()
        print(body)

        if not validate_json(body, JsonSchema.GET_COMPONENT_SCHEMA, "body"):
            return {"error" : "not valid query"} #provvisorio, inoltre manca il campo "data" nel get_component_schema.json
        
        if not "configuration" in body:
            body["configuration"] = 0
        if not "sensors" in body:
            body["sensors"] = ["accelerometer"]
        if not "labels" in body:
            r = requests.get(url="http://localhost:5001/labels")
            body["labels"] = r.json()

        if not "data" in body: #La richiesta fatta non ha dati dell'utente da cui partire per l'addestramento
                               #vengono quindi usati i dati della piattaforma
            
            body_dict = dict()
            body_dict['query_gen'] = body
            body_dict['type'] = "model_get"

            print("CB body_dict " + str(body_dict))
 
            db_res = rpc_client.call(body_dict) #richiesta al db per vedere se esiste già un modello con la stessa query
            db_res = json.loads(db_res.decode())

            #Il framework utilizzato (TF --> tensorflow) per ora viene hardcodato
            #Questo parametro serve al classifier deployer per capire che tipo container avviare
            folder_name = "TF_" + (str)(datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f'))
            
            if db_res["results"] == "error": #query non trovata nel db, quindi devo generare il classificatore

                data = train_cycle(tf_format, folder_name, folder_name, body)

                if not data:
                    return {"error":"no data in db"}
                else:
                    zip_name = folder_name + ".zip"
                    '''try:
                            os.unlink(os.path.abspath(os.path.join("tmp_models_CB", folder_name) + ".zip"))
                    except OSError as e:
                        print ("Error: %s - %s." % (e.filename, e.strerror))'''#Lo zip viene eliminato nella funzione train_cycle
                    return Response(data, headers={
                        'Content-Type': 'application/zip',
                        'Content-Disposition': 'attachment; filename=%s;' % zip_name})
            else:
                print("Model found")

                data = zip_when_model_found(db_res, folder_name)
                
                zip_name = folder_name + ".zip"
                return Response(data, headers={
                    'Content-Type': 'application/zip',
                    'Content-Disposition': 'attachment; filename=%s;' % zip_name})
        else:
            #Addestrare classificatore con dati contenuti nel campo "data"
            return "Hello World"

#######################################################################################################################
#                                               MAIN
#######################################################################################################################
if __name__ == "__main__":
    thread = threading.Thread(target=flask_thread)
    thread.start()

    init_rabbitmq(config[Config.RABBIT_MQ_CONNECTION.value])
