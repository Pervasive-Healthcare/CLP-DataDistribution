from modelinterface import ModelInterface
import importlib
import tensorflow as tf
import sys

class ModelImplementation(ModelInterface):

	def init_model(self, model_path):
		global new_model
		try:
			new_model = tf.keras.models.load_model(model_path)
		except:
			print("Error")

	def predict_data(self, data_arr):
		return new_model.predict(data_arr)


