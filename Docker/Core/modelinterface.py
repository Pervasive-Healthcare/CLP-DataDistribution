from abc import ABC, abstractmethod

class ModelInterface(ABC):
	@abstractmethod
	def init_model(self, model_path):
		pass

	@abstractmethod
	def predict_data(self, data):
		pass