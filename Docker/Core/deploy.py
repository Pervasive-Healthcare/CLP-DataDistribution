from flask import Flask, request, jsonify
import numpy as np
import importlib
import os

app = Flask(__name__)

def import_file(file_name):
	path = ""
	try:
		spec = importlib.util.spec_from_file_location(
			file_name + "model", path + file_name + "model.py"
		)
	except FileNotFoundError:
		return "File " + file_name + "model.py not found in directory: " + path

	mod = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(mod)
	return mod.ModelImplementation()

@app.route("/predict", methods=["POST"])
def predict():
	try:
		data_to_predict = request.get_json()
		if "data" in data_to_predict:
			data_arr = np.array(data_to_predict.get("data"))
			res = model.predict_data(data_arr.reshape(len(data_arr),150, 1, 3))
			response = {"status":"success", "predict": res.tolist()}
		else:
			response = {"status":"fail"}
	except Exception as e:
		print(e)
		response = {"status":"fail"}
	finally:
		return jsonify(response)


if __name__ == "__main__":
	global model
	path = os.getenv("MODEL_PATH")
	typemodel = os.getenv("MODEL_TYPE")
	print(path)
	print(typemodel)
	model = import_file(typemodel)
	model.init_model(path)
	app.run(host="0.0.0.0", port=5050)