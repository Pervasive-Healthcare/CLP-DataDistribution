# CLP - Data Distribution

The **Data Distribution** component provides   the   sets   of   signalslabelled   according   to   specific   needs,   the   trained   classifiers   andthe  labels  corresponding  to  activities  from  the  signals

[<img src="/data-distribution-arch__1_.png" width="550"/>](/data-distribution-arch__1_.png)

The component includes the following modules:
- **Classifier builder**: allows users to request to download the activity classifier already trained with the platform data. In this way, it is possible to obtain and integrate the classifier in any type of application and domain that requires it. The classifier improves automatically by re-training with the insertion of new datasets, it is advisable to keep the model always updated to the latest version by re-downloading it;
- **Classifier deployer**: takes care of saving the model pulled by the Classifier Builder and which will be used by the online classifier;
- **Online classifier**: the user is able to enter the unlabeled time series and obtain the labels for each input series in output. It is a real-time service that allows the user not to necessarily have to download the model and run it in order to use its features;
- **Query builder**: is responsible for downloading standardized platform data by users. It will provide users with the ability to have access to standardized data. In particular, it will be possible to query the database based on parameters to obtain only certain data if necessary.

The Data Distribution communicates through the RabbitMQ message broker, with the Repository Manager component, both to send requests and to receive the time series to be processed, and with the Web Application, through REST API calls.


## APIs [TODO]

