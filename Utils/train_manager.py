from tensorflow import keras as k
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split

from keras.callbacks import EarlyStopping

import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from datetime import datetime #serve solo per il salvataggio dell'imagine dei grafici

#funzione per suddivisione dei dati in input
def split_input(sensor, j_file, window_size=150, perc_test=0.2):
  X_app = []
  y_app = []

  #La percentuale di dati per il test non può ovviamente essere minore di 0
  #ma neanche maggiore di 0.5 perchè altrimenti verrebbero usati troppi dati per
  #il test e non per il training
  if perc_test <= 0 or perc_test > 0.5:
    perc_test = 0.2
    print("Percentuale di valori per il test non valida, settato valore di default 0.2")

  for i in j_file["data"]:
    start = 0 #start: variabile di inizio finestra
    j = start #j: variabile che scorre la finestra
    end = window_size - 1 #end: variabile di fine finestra
    tmp = []
    finish = False

    if len(i[sensor]) < window_size: #controllo se l'array del sensore ha lunghezza minore della finestra
        diff = window_size - len(i[sensor])
        x = 0
        first_elem = i[sensor][0]
        last_elem = i[sensor][len(i[sensor]) - 1]
        while x < diff // 2: #inserisco in testa e in coda rispettivamente il primo e l'ultimo elemento
                             #tante volte quanto è la differenza di lunghezza
          i[sensor].insert(0, first_elem)
          i[sensor].append(last_elem)
          x += 1
        if diff % 2 != 0: #se la differenza fosse un valore dispari, il valore in più lo aggiungo in coda
          i[sensor].append(last_elem)

    while not finish:
      if j >= start and j <= end: #scorrimento all'interno della finestra
        tmp.append(i[sensor][j])
        j = j + 1
      else:
        start += window_size//2
        if end + window_size//2 <= len(i[sensor]) - 1: #controllo se ho ancora spazio nell'array
          end += window_size//2
        elif j <= len(i[sensor]) - 1: #controllo se esiste almeno un elemento a fine array che potrei perdere
                                      #dato che la lunghezza dell'array/window_size non è sempre una divisione senza resto
          end = len(i[sensor]) - 1
          start = end - window_size + 1
        else:
          finish = True
        j = start
        X_app.append(tmp)

        #capire a quale label sono associati i dati che sto considerando andando a vedere a quale posizione
        #dell'array "labels" nel json si trova il label
        end_check_labels = False
        index = 0
        while index <= len(j_file["labels"]) -1 and (not end_check_labels):
          if i["label"] == j_file["labels"][index]:
            y_app.append(index)
            end_check_labels = True
          else:
            index += 1

        tmp = []

  X = np.array(X_app)
  y = np.array(y_app)

  #Utilizzo della funzione train_test_split per splittare correttamente le X e le y
  #in modo tale da avere varietà tra i valori di train e i valori di test
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=perc_test)

  y_train = k.utils.to_categorical(y_train, len(j_file["labels"]))
  y_test = k.utils.to_categorical(y_test, len(j_file["labels"]))

  X_train = X_train.reshape(len(X_train),window_size, 1, 3)
  X_test = X_test.reshape(len(X_test),window_size, 1, 3)

  return X_train, X_test, y_train, y_test

#funzione che ritorna il modello addestrato
def build_model_response(X_train, X_test, y_train, y_test, n_classes, epochs=80):

    input_shape = X_train.shape[1:]

    model = Sequential()
    model.add(Conv2D(8, kernel_size=1, activation='relu',
                    input_shape=input_shape))
    model.add(Conv2D(4, kernel_size=1, activation='relu'))
    model.add(Flatten())
    model.add(Dense(n_classes, activation='softmax'))
    model.compile(optimizer='adam', 
                loss='categorical_crossentropy',
                metrics=['accuracy'])
    #model.summary()

    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, min_delta=0.001, patience=10)

    history = model.fit(X_train, y_train,
                        #batch_size=batch_size,
                        epochs=epochs,
                        verbose=1,
                        validation_split=0.2,
                        callbacks=[es])

    score = model.evaluate(X_test, y_test, verbose=0)
    print('\nTest loss:', score[0])
    print('Test accuracy:', score[1])

    '''plt.figure(1)

    plt.subplot(211)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper right')
    #plt.savefig("C:/Users/fed99/Desktop/CLP_train_test/image_loss.png")

    plt.subplot(212)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='lower right')

    plt.tight_layout()

    plt.savefig("your_save_path_and_file_name_" + (str)(datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f')) + ".png")
    plt.close('all')'''
    return model