import pika
import yaml
import json
import uuid
from Utils.enums import Config

class ModelRpcClient(object):
	with open("config.yml", "r") as yml_file:
			config = yaml.safe_load(yml_file)

	rabbitmq_config = config[Config.RABBIT_MQ_CONNECTION.value]
	def __init__(self):
		credentials = pika.PlainCredentials(self.rabbitmq_config.get('user'), self.rabbitmq_config.get('password'))
		parameters = pika.ConnectionParameters(self.rabbitmq_config.get('host'),
											self.rabbitmq_config.get('port'),
											'/',
											heartbeat=0,
											credentials=credentials)

		self.connection = pika.BlockingConnection(parameters)
		self.channel = self.connection.channel()
		self.callback_queue = 'reply_queue'
		self.channel.queue_declare(self.callback_queue)
		self.channel.basic_consume(queue=self.callback_queue,on_message_callback=self.on_response,auto_ack=True)


	def on_response(self, ch, method, props, body):
		if self.corr_id == props.correlation_id:
			self.response = body

	def call(self, query):
		self.response = None
		self.corr_id = str(uuid.uuid4())
		self.channel.basic_publish(
			exchange='',
			routing_key='req_model_to_repo',
			properties=pika.BasicProperties(
				reply_to=self.callback_queue,
				correlation_id=self.corr_id,
			),
			body=json.dumps(query))
		while self.response is None:
			self.connection.process_data_events()
		return self.response
