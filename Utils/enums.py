from enum import Enum


class Sensors(Enum):
    ACCELEROMETER = "accelerometer"
    LINEAR_ACCELEROMETER = "linear accelerometer"
    GYROSCOPE = "gyroscope"
    MAGNETOMETER = "magnetometer"
    ORIENTATION = "orientation"


class Uom(Enum):
    METERS_SQUARE_SECONDS = "m/s^2"
    RAD_SEC = "rad/sec"
    MICRO_TESLA = "uT"


class Config(Enum):
    RABBIT_MQ_CONNECTION = "rabbitmq_connection"
    MONGO_CONNECTION = "mongodb_connection"
    KAFKA_CONNECTION = "kafka_connection"
    DB_NAME = "db_name"
    HOST = "host"
    PORT = "port"

class DefaultGeneralQuery(Enum):
    DEFAULT_GENERAL_QUERY = {"configuration":0, "labels":["laying", "walking"], "sensors":["accelerometer"]}
    #{"configuration":0, "labels":["laying", "walking"], "sensors":["accelerometer"], "db_names":["MS"]}

