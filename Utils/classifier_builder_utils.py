import os
import json
import requests
from zipfile import ZipFile
from Utils.train_manager import split_input, build_model_response
import tensorflow as tf
import shutil

def zip_when_model_found(db_res, folder_name):
    query = db_res["model_info"]
    query["new_model"] = False
    model_name = db_res["model_info"]["model_name"]
                
    #salvataggio dello zip e del json
    with ZipFile(os.path.join("tmp_models_CB", folder_name) + ".zip", 'w') as zipobj:
        zipobj.writestr("query.json", json.dumps(query).encode('UTF-8'))

        abs_src = os.path.abspath("models")
        print(abs_src)
        for dirname, subdirs, files in os.walk("models"):
            for name in model_name:
                for filename in files:
                    if name == filename:
                        absname = os.path.abspath(os.path.join(dirname, filename))
                        arcname = absname[len(abs_src) + 1:]
                        print(absname)
                        print(arcname)
                        zipobj.write(absname, arcname)
    
    #salvataggio in una variabile del contenuto dello zip
    #ed eliminazione dello zip dal disco fisso
    with open(os.path.abspath(os.path.join("tmp_models_CB", folder_name) + ".zip"), 'rb') as f:
        data = f.readlines()
    try:
        os.unlink(os.path.abspath(os.path.join("tmp_models_CB", folder_name) + ".zip"))
    except OSError as e:
        print ("Error: %s - %s." % (e.filename, e.strerror))
    return data

def train_cycle(tf_format, folder_name, model_name, query):
    r = requests.post(url = "http://localhost:5003/download_data", json = query)
    down_link = r.json()["download_link"]

    r = requests.get(url=down_link)
    query_result = r.json()
    if query_result["data"] == []:
        return False
    else:
        #ciclo di addestramento
        for i in query_result["sensors"]:
            X_train, X_test, y_train, y_test = split_input(i["type"], query_result)

            print(X_train.shape)
            print(X_test.shape)
            print(y_train.shape)
            print(y_test.shape)

            model = build_model_response(X_train, X_test, y_train, y_test, len(query_result["labels"]))

            if tf_format == '0': #0 --> formato tensorflow normale
                save_path = os.path.join("tmp_models_CB", folder_name, model_name + "_" + i["type"] + ".h5")
                model.save(save_path)
            elif tf_format == '1': #1 --> formato tensorflow lite per mobile
                converter = tf.lite.TFLiteConverter.from_keras_model(model)
                tflite_model = converter.convert()
                save_path = os.path.join("tmp_models_CB", folder_name)
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                with open(os.path.abspath(os.path.join(save_path, model_name + "_" + i["type"] + ".tflite")), 'wb') as f:
                    f.write(tflite_model)
        
        query["new_model"] = True
        #salvataggio json della query
        with open(os.path.join("tmp_models_CB", folder_name, "query.json"), 'w') as json_file:
            json.dump(query, json_file)

        #salvataggio dello zip
        with ZipFile(os.path.join("tmp_models_CB", folder_name) + ".zip", 'w') as zipobj:
            abs_src = os.path.abspath(os.path.join("tmp_models_CB", folder_name))
            print(abs_src)
            for dirname, subdirs, files in os.walk(os.path.join("tmp_models_CB", folder_name)):
                for filename in files:
                    absname = os.path.abspath(os.path.join(dirname, filename))
                    arcname = absname[len(abs_src) + 1:]
                    print(absname)
                    print(arcname)
                    zipobj.write(absname, arcname)
        
        work_dir = os.path.join(os.getcwd(), "tmp_models_CB")

        #rimozione cartella che ormai è stata zippata
        try:
            shutil.rmtree(os.path.join(work_dir, folder_name))
        except OSError as e:
            print ("Error: %s - %s." % (e.filename, e.strerror))

        #salvataggio in una variabile del contenuto dello zip
        #ed eliminazione dello zip dal disco fisso
        with open(os.path.abspath(os.path.join("tmp_models_CB", folder_name) + ".zip"), 'rb') as f:
            data = f.readlines()
        
        try:
            os.unlink(os.path.abspath(os.path.join("tmp_models_CB", folder_name) + ".zip"))
        except OSError as e:
            print ("Error: %s - %s." % (e.filename, e.strerror))
        return data