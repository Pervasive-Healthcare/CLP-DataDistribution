import docker
import os
from datetime import datetime

def init_docker_client():
	global client
	client = docker.from_env()

def get_image(imageId):
	try:
		image = client.images.get(imageId)
	except docker.errors.ImageNotFound as e:
		print(e)
		image = None
	except docker.errors.APIError as e:
		print(e)
		return False
	return image

def get_container(contId):
	try:
		cont = client.containers.get(contId)
	except docker.errors.NotFound as e:
		print(e)
		cont = None
	except docker.errors.APIError as e:
		print(e)
		return False
	return cont

def build_image(model_type):
	tag_name = "image/" + model_type
	args = dict()
	args["REQUIREMENTS"] =  "requirements/" +  "requirements" + model_type + ".txt"
	args["MODEL_IMPL"] = model_type.upper() + "model.py"
	try:
		path = "Docker"
		img = client.images.build(path=path,tag=tag_name,rm=True,forcerm=True,buildargs=args)
	except docker.errors.BuildError as e:
		print(e)
		return None
	except docker.errors.APIError as e:
		print(e)
		return None
	return img 

def run_container(image, env, ports):
	if get_image(image):
		pass
	else:
		build_image(image.split("/")[1])

	try:
		vol = {os.path.abspath("models"): {'bind': '/app/models', 'mode': 'ro'}}
		cont = client.containers.run(image,auto_remove=True,detach=True,environment=env,ports=ports,remove=True,volumes=vol)
	except docker.errors.ImageNotFound as e:
		print(e)
		return None
	except docker.errors.ContainerError as e:
		print(e)
		return None
	except docker.errors.APIError as e:
		print(e)
		return None
	return cont

def list_image():
	try:
		images = client.images.list()
		images_dict = dict()
		for x in images:
			images_dict[x.tags[0]] = {"id":x.id,"labels":x.labels,"short_id":x.short_id,"tags":x.tags,"image_obj":x}
		return images_dict
	except docker.errors.APIError as e:
		print(e)
		return None

def list_container(all_container):
	try:
		cont =  client.containers.list(all=all_container)
		cont_dict = dict()
		for x in cont:
			cont_dict[x.name] = {"id":x.id,"name":x.name,"image_obj":x.image,"cont_obj":x,
									"labels:":x.labels,"short_id":x.short_id,"status":x.status}
		return cont_dict
	except docker.errors.APIError as e:
		print(e)
		return None

def remove_image(imgName):
	try:
		client.images.remove(image=imgName, force=True)
	except docker.errors.ImageNotFound as e:
		print(e)
		return False
	except docker.errors.APIError as e:
		print(e)
		return False
	return True

def remove_container(containerId):
	try:
		contId = get_container(containerId)
		contId.remove()
	except docker.errors.APIError as e:
		print(e)
		return False
	return True

def stop_container(containerId):
	try:
		contId = get_container(containerId)
		contId.stop()
	except docker.errors.APIError as e:
		print(e)
		return False
	return True

def get_free_port(docker_config):
	for i in docker_config:
		if docker_config.get(i)[0]:
			return i
	return None

def docker_manager(docker_config, model_name, framework):
	if docker_config['running_cont'] >= docker_config['max_container']:
		listOfTuple = [(key, value[2]) for (key, value) in docker_config['ports'].items() if value[1] is not None]
		print(listOfTuple)
		key = [key for (key, value) in listOfTuple if value == min([value[2] for value in docker_config['ports'].values() if not value[0]])][0]
		print(key)
		print(docker_config['ports'].get(key)[1])
		if stop_container(docker_config['ports'].get(key)[1]):
			docker_config['running_cont'] -= 1
			docker_config['ports'][key][0] = True
			docker_config['ports'][key][1] = None
	
	key = get_free_port(docker_config['ports'])
	img = "image/" + framework.lower()
	env = ["MODEL_PATH=models/" + model_name, "MODEL_TYPE=" + framework.upper()]
	cont = run_container(img, env, {"5050":key})
	if key and cont:
		docker_config['running_cont'] += 1
		docker_config['ports'][key][0] = False
		docker_config['ports'][key][1] = cont.id
		docker_config['ports'][key][2] = datetime.timestamp(datetime.now())
		return (True, key)
	return (False, None)

def run_models(docker_config, list_models, framework):
	links = dict()
	for model_name in list_models:
		ris = docker_manager(docker_config,model_name,framework)
		if ris[0]:
			links[model_name] = "http://localhost:" + str(ris[1]) + "/predict"
		print(docker_config)
	return links

