import json
import os
from enum import Enum

from jsonschema import validate, ValidationError, SchemaError


class JsonSchema(Enum):
    QUERY_SCHEMA = "query_schema.json"
    DATA_SCHEMA = "data_schema.json"
    DBINFO_SCHEMA = "dbinfo_schema.json"
    CONFIG_SCHEMA = "config_schema.json"
    GET_COMPONENT_SCHEMA = "get_component_schema.json"


schemas = {}
head, tail = os.path.split(os.path.dirname(__file__))

with open(os.path.join(head, "Utils", "query_schema.json")) as json_file:
    schemas[JsonSchema.QUERY_SCHEMA] = json.load(json_file)
    json_file.close()

with open(os.path.join(head, "Utils", "get_component_schema.json")) as json_file:
    schemas[JsonSchema.GET_COMPONENT_SCHEMA] = json.load(json_file)
    json_file.close()


def validate_json(json_to_validate, json_schema, description):
    try:
        validate(instance=json_to_validate, schema=schemas[json_schema])
        return True
    except ValidationError:
        print("%s - ValidationError" % description)
        return False
    except SchemaError as e:
        print(e)
        print("%s - SchemaError" % description)
        return False



